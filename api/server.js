const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const urls = require('./app/urls');
const config = require('./config');

const port = 8000;
const app = express();

app.use(cors());
app.use(express.json());

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
    app.use('/', urls());
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
