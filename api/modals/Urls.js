const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const urlSchema = new Schema({
    shortUrl: {type: String, unique: true},
    originalUrl: {type: String, required : true}
});

const Urls = mongoose.model('Urls', urlSchema);

module.exports = Urls;