const express = require('express');
const nanoid = require('nanoid');
const Urls = require('../modals/Urls');


const router = express.Router();

const createRouter = () => {

    router.post('/', (req, res) => {
        const url = req.body;
        url.shortUrl = nanoid(7);
        const urls = new Urls(url);
        urls.save()
            .then(result => res.send('localhost:8000/' + result.shortUrl))
            .catch(() => res.sendStatus(500));
    });

    router.get('/:shortUrl', (req, res) => {
       Urls.findOne({shortUrl : req.params.shortUrl}, (error, result) => {
           if (error) res.status(404).send(error);
           if (result) res.status(301).redirect(result.originalUrl);
       })
    });

    return router;
};

module.exports = createRouter;