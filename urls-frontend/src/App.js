import React, { Component } from 'react';
import {Button, ControlLabel, Form, FormControl, FormGroup, HelpBlock} from "react-bootstrap";
import  './App.css';
import {connect} from "react-redux";
import {getShortUrl} from "./store/actions";


class App extends Component {

    state = {
        originalUrl: ''
    };

    inputChangeHandler = e => {
        this.setState({[e.target.name] : e.target.value})
    };

    formSubmit = e => {
        e.preventDefault();
        this.props.sendUrl(this.state)
    };

  render() {
    return (
      <div className="container">
          <Form onSubmit={this.formSubmit}>
              <FormGroup
              >
                  <ControlLabel>Insert your URL</ControlLabel>
                  <FormControl
                      type="text"
                      required
                      placeholder="Enter URL"
                      name='originalUrl'
                      value={this.state.originalUrl}
                      onChange={this.inputChangeHandler}
                  />
                  <FormControl.Feedback />
                  <Button
                      type='submit' className='button' bsStyle="primary">Get short URL</Button>
                  <HelpBlock>Your short URL will be here.</HelpBlock>

                  <FormControl
                      type="text"
                      placeholder=""
                      value={this.props.shortUrl}
                  />
              </FormGroup>
          </Form>
      </div>
    );
  }
}
const mapStateToProps = state => {
    return {
        shortUrl: state.url
    }
};
const mapDispachToProps = dispatch => {
    return {
        sendUrl: url => dispatch(getShortUrl(url))
    }
};

    export default connect(mapStateToProps, mapDispachToProps)(App);
