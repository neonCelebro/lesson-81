import {FETCH_ERROR, FETCH_RESPONSE} from "./actions";

const initialState = {
    url: '',
    error: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type){
        case FETCH_RESPONSE:
            console.log(action.url)
            return {...state, url: action.url};
        case FETCH_ERROR:
            return {...state, error: action.error};
        default: return state;
    }
};

export default reducer;