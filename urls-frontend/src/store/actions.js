import axios from '../axios';

export const FETCH_RESPONSE = 'FETCH_RESPONSE';
export const FETCH_ERROR = 'FETCH_ERROR';

const fetchResponse = url => {
    return {type: FETCH_RESPONSE, url}
};

const fetchError = error => {
    return {type: FETCH_ERROR, error}
};

export const getShortUrl = url => {
    return (dispatch) => {
        return axios.post('/', url).then(
            res => { console.log("response is " + res.data); dispatch(fetchResponse(res.data))},
            error => dispatch(console.log(error.data))
        )
    }
};